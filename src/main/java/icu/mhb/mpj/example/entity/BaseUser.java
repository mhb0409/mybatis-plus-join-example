package icu.mhb.mpj.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @author mahuibo
 * @Title: BaseUser
 * @email mhb0409@qq.com
 * @time 2024/7/8
 */
@Data
public class BaseUser implements Serializable {

    private static final long serialVersionUID = 42L;

    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

}
